public class StringMethod {
    public static void main(String[] args) {
        String s="Toi yeu lap trinh";

        //1: length() trả về độ dài của chuỗi string s
        System.out.println(s.length()); //17

        //2: charAt(): trả về 1 kí tự trong chuỗi
        System.out.println(s.charAt(1)); // o

        //3: replace(): thay thế các chuỗi/kí tự được tìm thấy thành chuỗi/kí tự khác
        System.out.println(s.replace('i','y')); //Toy yeu lap trynh
        System.out.println(s.replace("yeu","ghet")); //Toi ghet lap trinh

        //4: toUpperCase(),toLowerCase(): chuyển kí tự của 1 xâu từ in hoa về thường hoặc ngược lại
        System.out.println(s.toLowerCase()); //toi yeu lap trinh
        System.out.println(s.toUpperCase()); //TOI YEU LAP TRINH

        //5: indexOf(): trả về vị trí xuất hiện đầu tiên của 1 String trong 1 String khác, ko tìm thấy return -1
        System.out.println(s.indexOf("yeu")); //4
        System.out.println(s.indexOf("ghet")); //-1

        //6: startsWith(), endsWith(): kiểm tra một xâu có bắt đầu hoặc kết thúc băng một xâu khác không.
        System.out.println(s.startsWith("Toi"));//true
        System.out.println(s.startsWith("Tui"));//false
        System.out.println(s.endsWith("trinh"));//true
        System.out.println(s.endsWith("trynh"));//false

        //7: split(): tách 1 xâu thành 1 mảng các xâu dựa trên 1 xâu cho trước
        String []word=s.split(" " );
        for(int i=0;i<word.length;i++){
        }

        //8: subString(): cắt ra 1 xâu con từ xâu s dựa trên chỉ số bắt đầu, chỉ số kết thúc của 1 xâu khác
        System.out.println(s.substring(0, 4));//Toi_: cắt từ vị trí thứ 0 của xâu s, độ dài 4 phần tử
        System.out.println(s.substring(4)); //yeu lap trinh: cắt từ vị trí thứ 4 đến hết

        //9:  compareTo() so sánh các chuỗi cho trước với chuỗi hiện tại theo thứ tự từ điển. Nó trả về số dương, số âm hoặc 0.
        //Nếu chuỗi đầu tiên lớn hơn chuỗi thứ hai, nó sẽ trả về số dương (chênh lệch giá trị ký tự).
        // Nếu chuỗi đầu tiên nhỏ hơn chuỗi thứ hai, nó sẽ trả về số âm
        // và nếu chuỗi đầu tiên là bằng chuỗi thứ hai, nó trả về 0.
        String MSSV="DE130047";
        System.out.println(MSSV.compareTo("DE130048")); //-1
        System.out.println(MSSV.compareTo("DE130046")); //1
        System.out.println(MSSV.compareTo("DE130047"));//0

        //10: concat() nối thêm chuỗi được chỉ định vào cuối chuỗi đã cho.
        s=s.concat(" haha");
        System.out.println(s); //Toi yeu lap trinh haha

        //11: contains():tìm kiếm chuỗi ký tự,nó trả về true nếu chuỗi các giá trị char được tìm thấy trong chuỗi này,
        // nếu không trả về false., có phân biệt chữ thường chữ hoa
        System.out.println(s.contains("Toi"));//true
        System.out.println(s.contains("trynh"));//false

        //12: equals():so sánh hai chuỗi đưa ra dựa trên nội dung của chuỗi. Nếu hai chuỗi khác nhau nó trả về false.
        // Nếu hai chuỗi bằng nhau nó trả về true., phân biệt chữ hoa chữ thường
        String s2="java is OOP";
        System.out.println(s2.equals("java is OOP"));//true
        System.out.println(s2.equals("java")); //false

        //13: equalsIgnoreCase() tương tự như equals() nhưng không phân biệt chữ hoa và chữ thường.

        //14: isEmpty(): khi chuỗi trống trả về true, ngược lại return false;
        System.out.println(s.isEmpty()); //false

        //15: trim(): xóa khoảng trắng đầu và cuối của xâu
        //16: valueOf(): được sử dụng để chuyển đối kiểu dữ liệu khác thành chuỗi.
        int n=13;
        String ns=String.valueOf(n);
        System.out.println(ns+31); //1331

    }
}
