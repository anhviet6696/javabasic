public class checkPalin {
    // kiểm tra xâu kí tự dù viết ngược hay viết xuôi vẫn cho ra kết quả giống nhau
    boolean CheckPalin(String s){
        int n=s.length();
        char [] str=new char[n];
        for(int i=0;i<n;i++){
            str[i]=s.charAt(i);
        }
        for(int i=0;i<n/2;i++){
            if(str[i]!=str[n-i-1])
                return false;

        }
        return true;
    }

    public static void main(String[] args) {
        checkPalin check=new checkPalin();
        System.out.println(check.CheckPalin("abbaa"));
    }
}
