public class formatString {
    String format(String s) {
        return s.replaceAll("\\s+", " ").trim();

    }

    /*String truncateString(String s) {
        char[] str = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            str[i] = s.charAt(i);
        }
        while (s.length() > 0) {
            if ((str[0] - '0') % 3 == 0) {
                s = s.substring(1, s.length());
            } else if ((str[s.length() - 1]) % 3 == 0) {
                s = s.substring(0, s.length() - 1);
            } else if (((str[0] + str[s.length() - 1] - 96) % 3 == 0)) {
                s = s.substring(1, s.length() - 1);
            }

        }
        return s;
    }*/
    String TruncateString(String s) {
        if (s.length() == 0) {
            return s;
        }
        char[] str = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            str[i] = s.charAt(i);
        }
        int first = str[0], last = str[s.length() - 1];
        if (first % 3 == 0) {
            s = s.substring(0, 1);
            return TruncateString(s);
        } else if (last % 3 == 0) {
            s = s.substring(s.length() - 1, 1);
            return TruncateString(s);
        } else if ((first + last) % 3 == 0) {
            s = s.substring(0, 1);
            s = s.substring(s.length() - 1, 1);
            return TruncateString(s);
        } else
            return s;
    }

    public static void main(String[] args) {
        formatString format = new formatString();
        //System.out.println(format.format("abc   a aa a a"));
        //System.out.println(format.TruncateString("312248"));
    }
}