public class StrongPassword {
    boolean StrongPassword(String password){
        if(password.length()<6)
            return false;
        boolean checkUpper=false;
        boolean checkLower=false;
        boolean checkNumber=false;
        boolean checkSpecialCha=false;
        for (int i=0; i<password.length(); i++)
        {
            if(password.charAt(i)>=65 && password.charAt(i)<=90)
            //if(password.charAt(i)>='A' && password.charAt(i)<='Z')
                checkUpper=true;
            if (password.charAt(i) >= 97 && password.charAt(i) <= 122)
                checkLower = true;
            if (password.charAt(i) >= 48 && password.charAt(i) <= 57)
                checkNumber = true;
            if (password.charAt(i) >='!' && password.charAt(i)<='+')
                checkSpecialCha = true;
        }
        return checkLower&&checkNumber&&checkUpper&&checkSpecialCha;
    }

    public static void main(String[] args) {
        StrongPassword pass=new StrongPassword();
        System.out.println(pass.StrongPassword("123456Viet!"));;
    }
}
