import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class lineEncoding {
    String lineEncoding(String s) {
        int count = 1;
        String rs = "";
        char[] a = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            a[i] = s.charAt(i);
        }
        for (int j = 0; j < s.length() - 1; j++) {
            if (a[j] == a[j + 1]) {
                count++;
            } else if (a[j] != a[j + 1]) {
                if (count == 1) {
                    rs = rs + a[j];
                } else
                    rs = rs + count + "" + a[j];
                count = 1;
            }

        }
        if (a[s.length() - 1] != a[s.length() - 2]) {
            rs = rs + a[s.length() - 1];
            count = 1;
        } else if (a[s.length() - 1] == a[s.length() - 2]) {
            count++;
            rs = rs + --count + "" + a[s.length() - 1];
        }
        return rs;
        /*StringBuilder builder = new StringBuilder(s);
        Pattern pattern = Pattern.compile("(\\w)\\1+");
        Matcher matcher = pattern.matcher(builder);
        while(matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            int occurance = end - start;
            builder = builder.replace(start, end-1, String.valueOf(occurance));
            matcher = pattern.matcher(builder);
        }
        return builder.toString();*/
    }

    String toString(int n) {
        String s = "";
        while (n > 0) {
            s = (char) (n % 10 + '0') + s;
            n /= 10;
        }
        return s;
    }

    public static void main(String[] args) {
        lineEncoding line = new lineEncoding();
        System.out.println(line.lineEncoding("wwwwwwwawwwwwww"));
        //System.out.println(line.toString(3));

    }
}
