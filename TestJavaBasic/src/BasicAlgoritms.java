import java.util.Random;
import java.util.Scanner;

public class BasicAlgoritms {

    void Display(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
        }
    }

    int[] BubbleSort(int[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a;
    }

    int[] SelectionSort(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            int minValue = a[i];
            int minIndex = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < minValue) {
                    minValue = a[j];
                    minIndex = j;
                }
            }
            int temp = a[minIndex];
            a[minIndex] = a[i];
            a[i] = temp;
        }
        return a;
    }

    /*int [] InsertionSort (int []a){
        for(int i=1;i<a.length;i++){
            for(int j=i-1;j>=0;j--){
                if(a[j+1]<a[j]){
                    int temp=a[j+1];
                    a[j+1]=a[j];
                    a[j]=temp;
                }
            }
        }
        return a;
    }*/
    int[] InsertionSort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            int j = i - 1;
            int current=a[i];
            while (j >= 0) {
                if (a[j] > current) {
                    int temp = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = temp;
                }
                j--;
            }
        }
        return a;
    }

    public static void main(String[] args) {
        Random rand = new Random();
        int[] arr = new int[6];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(10);
        }
        BasicAlgoritms algo = new BasicAlgoritms();
        algo.Display(arr);
        System.out.println();
        algo.InsertionSort(arr);
        algo.Display(arr);
    }
}
