public class AmendSentence {
    // chuỗi AnhVietDay => anh viet day
    String amendTheSentence(String s){
        String result="";
        //create new char[] to store String s
        char [] str=new char[s.length()];
        for(int i=0;i<s.length();i++){
            str[i]=s.charAt(i);
        }
        // check first letter to lowercase
        if(str[0]>='A' && str[0]<='Z')
            str[0]+=32;
        for(int i=0;i<s.length();i++){
            if(str[i]>='A' && str[i]<='Z'){
                result = result + " " +(char)(str[i]+32);
            }else
                result=result+str[i];
        }
        return result;
    }

    public static void main(String[] args) {
        AmendSentence ams=new AmendSentence();
        System.out.println(ams.amendTheSentence("AnhVietDay"));
    }
}
