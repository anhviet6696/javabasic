import java.util.Scanner;

public class GreatestCommonDivisor {
    int UCLN(int a, int b){
        while(a!=b){
            if(a>b)
                a-=b;
            else
                b-=a;
        }
        return a;
    }
    int BCNN(int a, int b){
        return (a*b)/UCLN(a,b);
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please input a & b:");
        int a=scanner.nextInt();
        int b=scanner.nextInt();
        GreatestCommonDivisor gcd=new GreatestCommonDivisor();
        System.out.println(gcd.UCLN(a,b));
        System.out.println(gcd.BCNN(a,b));
    }
}
